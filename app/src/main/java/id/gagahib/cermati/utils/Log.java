package id.gagahib.cermati.utils;


import id.gagahib.cermati.BuildConfig;

import static id.gagahib.cermati.utils.MyConfig.APP_TAG;

public class Log {
    
    private static boolean isShow = BuildConfig.DEBUG;
    
    public static void d(String tag, String value){
        if (value != null && isShow){
            android.util.Log.d(APP_TAG+tag, value);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void d(String tag, Integer value){
        if (value != null && isShow){
            android.util.Log.d(APP_TAG+tag, String.valueOf(value));
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void e(String tag, String value){
        if (value != null && isShow){
            android.util.Log.e(APP_TAG+tag, value);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void e(String tag, String value, Throwable e){
        if (value != null && isShow){
            android.util.Log.e(APP_TAG+tag, ""+value, e);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void w(String tag, String value){
        if (value != null && isShow){
            android.util.Log.w(APP_TAG+tag, value);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void w(String tag, String value, Throwable e){
        if (value != null && isShow){
            android.util.Log.e(APP_TAG+tag, ""+value, e);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void v(String tag, String value){
        if (value != null && isShow){
            android.util.Log.v(APP_TAG+tag, value);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }

    public static void i(String tag, String value){
        if (value != null && isShow){
            android.util.Log.i(APP_TAG+tag, value);
        }else{
            android.util.Log.e(APP_TAG+tag, "was null");
        }
    }
}
