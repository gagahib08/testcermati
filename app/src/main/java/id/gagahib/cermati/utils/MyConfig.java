package id.gagahib.cermati.utils;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import id.gagahib.cermati.R;

public class MyConfig {
    public static String APP_TAG = "Cermati";
    public static String TOKEN = "b61aee8c908937beb403ca81561913929d1c04fa";


    public static void printLog(String name, String message){

        if (message.contains("API rate limit exceeded")){
            message = "API rate limit exceeded, please wait for 60 second.";
        }else if (message.contains("Validation Failed")){
            message = "Validation Failed";
        }
        Log.e(APP_TAG+name, message);

    }


    public static void printLog(Context context, String name, String message){

        Log.e(APP_TAG+name, message);
        if (message.contains("API rate limit exceeded")){
            message = "API rate limit exceeded, please wait for 60 second.";
        }else if (message.contains("Validation Failed")){
            message = "Validation Failed";
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


    }


    public static void startShimmer(ShimmerFrameLayout sfLayout){
        if (sfLayout != null){
            sfLayout.startShimmer();
            sfLayout.setVisibility(View.VISIBLE);
        }
    }

    public static void startShimmer(ShimmerFrameLayout sfLayout, LinearLayout linLayout){
        if (sfLayout != null){
            sfLayout.startShimmer();
            sfLayout.setVisibility(View.VISIBLE);
            linLayout.setVisibility(View.GONE);
        }
    }

    public static void stopShimmer(ShimmerFrameLayout sfLayout){
        if (sfLayout != null){
            sfLayout.stopShimmer();
            sfLayout.setVisibility(View.GONE);
        }
    }

    public static void stopShimmer(ShimmerFrameLayout sfLayout, LinearLayout linLayout){
        if (sfLayout != null){
            sfLayout.stopShimmer();
            sfLayout.setVisibility(View.GONE);
            linLayout.setVisibility(View.VISIBLE);
        }
    }

}
