package id.gagahib.cermati.ui.customs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CustomTextView extends AppCompatTextView {
    public CustomTextView(Context context) {
        super(context);
        setFont();
    }
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text==null?"":text, type);
    }

    private void setFont() {
//        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "KGPrimaryPenmanship.ttf");
//        setTypeface(font, Typeface.NORMAL);
    }

}
