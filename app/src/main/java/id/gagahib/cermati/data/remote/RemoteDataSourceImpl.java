package id.gagahib.cermati.data.remote;

import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.payload.GitPayload;
import id.gagahib.cermati.data.repository.RemoteDataSource;
import id.gagahib.cermati.utils.Log;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteDataSourceImpl extends BaseRemoteDataSource implements RemoteDataSource {


    @Override
    public void doGetUsers(GitPayload payload, final OnGetUsers onGetUsers) {
        Call<GitBean> call = remoteDataService.doGetUsers(payload.getQ(), payload.getPage());
        call.enqueue(new Callback<GitBean>() {
            @Override
            public void onResponse(Call<GitBean> call, Response<GitBean> response) {
                GitBean gitBean = response.body();
                try {
                    if (response.isSuccessful()) {
                        onGetUsers.onSuccess(gitBean);
                    } else {
                        onGetUsers.onFailed(response.errorBody().string());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onGetUsers.onFailed(e.toString());
                }
            }

            @Override
            public void onFailure(Call<GitBean> call, Throwable t) {
                onGetUsers.onFailed(t.toString());
            }
        });
    }
}
