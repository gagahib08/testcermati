package id.gagahib.cermati.data.beanmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GitBean extends BaseDataBean{

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("incomplete_results")
    private Boolean incompleteResults;

    @SerializedName("items")
    private List<UserGitDataBean> items;

    public Integer getTotalCount() {
        return totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public List<UserGitDataBean> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return "GitBean{" +
                "totalCount=" + totalCount +
                ", incompleteResults=" + incompleteResults +
                ", items=" + items +
                '}';
    }
}
