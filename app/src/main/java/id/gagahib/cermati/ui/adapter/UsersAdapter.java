package id.gagahib.cermati.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import id.gagahib.cermati.R;
import id.gagahib.cermati.data.beanmodel.UserGitDataBean;
import id.gagahib.cermati.ui.customs.CustomTextView;
import id.gagahib.cermati.utils.GlideApp;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private List<UserGitDataBean> userGitDataBeans;
    private FragmentActivity context;

    public UsersAdapter(FragmentActivity context, List<UserGitDataBean> UserGitDataBeans){
        this.userGitDataBeans = UserGitDataBeans;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_user, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        UserGitDataBean userGitDataBean = userGitDataBeans.get(i);
        viewHolder.tvUserName.setText(userGitDataBean.getLogin());
        GlideApp.with(context)
                .load(userGitDataBean.getAvatarUrl())
                .apply(new RequestOptions().placeholder(R.color.colorGray2))
                .into(viewHolder.ivUserImage);

        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return userGitDataBeans.size();
    }

    public void add(int position, UserGitDataBean userGitDataBean) {
        userGitDataBeans.add(position, userGitDataBean);
        notifyItemInserted(position);
    }

    public void addAll(List<UserGitDataBean> userGitDataBeans) {
        userGitDataBeans.addAll(userGitDataBeans);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        userGitDataBeans.remove(position);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View layout;
        public ImageView ivUserImage;
        CustomTextView tvUserName;
        public ViewHolder(View v) {
            super(v);
            layout = v;
            ivUserImage = v.findViewById(R.id.ivUserImage);
            tvUserName = v.findViewById(R.id.tvUserName);
        }
    }
}
