package id.gagahib.cermati.data.beanmodel;

import com.google.gson.annotations.SerializedName;

public class UserGitDataBean extends BaseDataBean{


    @SerializedName("login")
    private String login;

    @SerializedName("id")
    private Integer id;

    @SerializedName("node_id")
    private String nodeId;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public Integer getId() {
        return id;
    }

    public String getNodeId() {
        return nodeId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
