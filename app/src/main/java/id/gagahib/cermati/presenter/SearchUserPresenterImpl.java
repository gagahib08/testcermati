package id.gagahib.cermati.presenter;

import androidx.annotation.NonNull;

import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.payload.GitPayload;
import id.gagahib.cermati.data.repository.RemoteDataRepository;
import id.gagahib.cermati.data.repository.RemoteDataSource;
import id.gagahib.cermati.ui.views.SearchUserView;

public class SearchUserPresenterImpl implements SearchUserPresenter{

    private SearchUserView presenterView;
    private final RemoteDataRepository remoteDataRepository;

    public SearchUserPresenterImpl(@NonNull RemoteDataRepository remoteDataRepository, @NonNull SearchUserView presenterView) {
        this.remoteDataRepository = remoteDataRepository;
        this.presenterView = presenterView;
        presenterView.setPresenter(this);
    }

    @Override
    public void start() {
    }

    @Override
    public void doGetUsers(GitPayload payload) {
        remoteDataRepository.doGetUsers(payload, new RemoteDataSource.OnGetUsers() {
            @Override
            public void onSuccess(GitBean gitBean) {
                presenterView.onGetUsers(gitBean);
            }

            @Override
            public void onFailed(String message) {
                presenterView.onFailedGetUsers(message);
            }
        });
    }
}
