package id.gagahib.cermati;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import id.gagahib.cermati.base.BaseActivity;
import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.beanmodel.UserGitDataBean;
import id.gagahib.cermati.data.payload.GitPayload;
import id.gagahib.cermati.presenter.SearchUserPresenter;
import id.gagahib.cermati.presenter.SearchUserPresenterImpl;
import id.gagahib.cermati.ui.adapter.UsersAdapter;
import id.gagahib.cermati.ui.customs.CustomEditText;
import id.gagahib.cermati.ui.customs.CustomTextView;
import id.gagahib.cermati.ui.views.SearchUserView;
import id.gagahib.cermati.utils.Log;
import id.gagahib.cermati.utils.MyConfig;

public class MainActivity extends BaseActivity implements SearchUserView {

    SearchUserPresenter presenter;
    int currentPage = 1;
    CustomEditText etSearch;
    CustomTextView tvNotFound;
    NestedScrollView scrollView;
    ShimmerFrameLayout sfLayout, scrollSfLayout;
    LinearLayout linLayout;
    RecyclerView reviUsers;
    List<UserGitDataBean> userGits;
    UsersAdapter usersAdapter;
    FragmentActivity activity;
    boolean isNewKeyword = true;
    boolean isStopScroll = false;
    private Timer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        sfLayout = findViewById(R.id.sfLayout);
        scrollSfLayout = findViewById(R.id.scrollSfLayout);
        linLayout = findViewById(R.id.linLayout);
        etSearch = findViewById(R.id.etSearch);
        tvNotFound = findViewById(R.id.tvNotFound);
        scrollView = findViewById(R.id.scrollView);
        reviUsers = findViewById(R.id.reviUsers);

        userGits = new ArrayList<>();
        usersAdapter = new UsersAdapter(activity, userGits);
        reviUsers.setLayoutManager(new LinearLayoutManager(activity));
        reviUsers.setAdapter(usersAdapter);

        presenter = new SearchUserPresenterImpl(getRemoteDataRepository(), this);

        initScrollListener();
        etSearch.addTextChangedListener(initTextChangeListener());
    }


    private TextWatcher initTextChangeListener(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new TimerTask() {
                            @Override
                            public void run() {
                                if (etSearch.getEditableText().toString().length() > 0){
                                    isNewKeyword = true;
                                    currentPage = 1;
                                    presenter.doGetUsers(initPayload(etSearch.getEditableText().toString()));
                                    MyConfig.startShimmer(sfLayout, linLayout);
                                }else{
                                    userGits.clear();
                                    usersAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                }, 600);
            }
        };
    }

    private GitPayload initPayload(String keyword){
        GitPayload payload = new GitPayload();
        payload.setPage(currentPage);
        payload.setQ(keyword);
        return payload;
    }

    @Override
    public void onGetUsers(GitBean gitBean) {
        if (isNewKeyword){
            userGits.clear();
            isStopScroll = false;
        }
        Log.d("TotalCount", gitBean.getTotalCount());
        userGits.addAll(gitBean.getItems());
        usersAdapter.notifyDataSetChanged();

        tvNotFound.setVisibility(View.GONE);

        if (gitBean.getItems().size() == 0){
            isStopScroll = true;
            if (gitBean.getTotalCount() == 0){
                tvNotFound.setVisibility(View.VISIBLE);
                tvNotFound.setText("\'"+etSearch.getEditableText().toString()+"\' "+getString(R.string.text_not_found));
            }else{
                Toast.makeText(activity, getString(R.string.text_no_more_result), Toast.LENGTH_SHORT).show();
            }
        }else{
            currentPage++;
        }

        MyConfig.stopShimmer(sfLayout, linLayout);
        MyConfig.stopShimmer(scrollSfLayout);
    }

    @Override
    public void onFailedGetUsers(String message) {
        MyConfig.printLog(activity, "SearchUser", message);

        MyConfig.stopShimmer(sfLayout, linLayout);
        MyConfig.stopShimmer(scrollSfLayout);
    }

    @Override
    public void setPresenter(SearchUserPresenter presenter) {
    }

    private void initScrollListener(){
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(userGits.size() > 0 ){
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                scrollY > oldScrollY) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isStopScroll){
                                        isNewKeyword = false;
                                        presenter.doGetUsers(initPayload(etSearch.getEditableText().toString()));
                                        MyConfig.startShimmer(scrollSfLayout);
                                    }
                                }
                            }, 100);
                        }
                    }
                }
            }
        });
    }

}