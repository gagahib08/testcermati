package id.gagahib.cermati.data.repository;

import androidx.annotation.NonNull;

import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.payload.GitPayload;

import static com.bumptech.glide.util.Preconditions.checkNotNull;

public class RemoteDataRepository implements RemoteDataSource {
    private final RemoteDataSource mainRemoteDataRepository;
    private static RemoteDataRepository instance = null;

    public RemoteDataRepository(@NonNull RemoteDataSource mainRemoteDataRepository){
        this.mainRemoteDataRepository  = checkNotNull(mainRemoteDataRepository);
    }

    public static RemoteDataRepository getInstance(RemoteDataSource mainRemoteDataSource){
        if(instance == null){
            instance = new RemoteDataRepository(mainRemoteDataSource);
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    @Override
    public void doGetUsers(GitPayload payload, final OnGetUsers onGetUsers) {
        mainRemoteDataRepository.doGetUsers(payload, new OnGetUsers() {
            @Override
            public void onSuccess(GitBean gitBean) {
                onGetUsers.onSuccess(gitBean);
            }

            @Override
            public void onFailed(String message) {
                onGetUsers.onFailed(message);
            }
        });
    }
}
