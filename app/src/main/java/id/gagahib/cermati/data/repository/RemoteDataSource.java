package id.gagahib.cermati.data.repository;
import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.payload.GitPayload;


public interface RemoteDataSource {

    void doGetUsers(GitPayload payload, OnGetUsers onGetUsers);
    interface OnGetUsers{
        void onSuccess(GitBean gitBean);
        void onFailed(String message);
    }
}
