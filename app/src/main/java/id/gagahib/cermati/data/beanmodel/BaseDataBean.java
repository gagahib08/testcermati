package id.gagahib.cermati.data.beanmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseDataBean {

    @SerializedName("documentation_url")
    private String documentationUrl;

    @SerializedName("message")
    private String message;

    @SerializedName("errors")
    private List<ErrorGitBean> errors;

    public String getDocumentationUrl() {
        return documentationUrl;
    }

    public String getMessage() {
        return message;
    }

    public List<ErrorGitBean> getErrors() {
        return errors;
    }
}
