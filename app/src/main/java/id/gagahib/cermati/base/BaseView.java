package id.gagahib.cermati.base;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
