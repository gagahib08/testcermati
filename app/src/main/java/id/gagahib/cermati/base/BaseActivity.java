package id.gagahib.cermati.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import id.gagahib.cermati.R;
import id.gagahib.cermati.data.remote.RemoteDataSourceImpl;
import id.gagahib.cermati.data.repository.RemoteDataRepository;

public class BaseActivity extends AppCompatActivity {
    private RemoteDataSourceImpl mainRemoteDataSource;
    private RemoteDataRepository mainDataRepository;
    protected ProgressDialog progressDialog;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainRemoteDataSource = new RemoteDataSourceImpl();
        mainDataRepository = RemoteDataRepository.getInstance(mainRemoteDataSource);
        setDialog();
    }

    protected RemoteDataRepository getRemoteDataRepository() {
        return mainDataRepository;
    }

    private void setDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.text_loading_please_wait));
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    protected void initLoadingDialog(){
        progressDialog.show();
    }

    protected void closeLoadingDialog() {
        if (progressDialog != null) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                }
            }, 1000);
        }
    }


}
