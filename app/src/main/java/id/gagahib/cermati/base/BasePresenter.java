package id.gagahib.cermati.base;

public interface BasePresenter {
    void start();
}
