package id.gagahib.cermati.ui.views;

import id.gagahib.cermati.base.BaseView;
import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.presenter.SearchUserPresenter;

public interface SearchUserView extends BaseView<SearchUserPresenter> {
    void onGetUsers(GitBean gitBean);
    void onFailedGetUsers(String message);


}
