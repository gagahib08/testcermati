package id.gagahib.cermati.ui.customs;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);
        setFont();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text==null?"":text, type);
    }

    private void setFont() {
//        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "KGPrimaryPenmanship.ttf");
//        setTypeface(font, Typeface.NORMAL);
    }
}
