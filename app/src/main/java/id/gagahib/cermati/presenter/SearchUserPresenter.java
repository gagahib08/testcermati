package id.gagahib.cermati.presenter;

import id.gagahib.cermati.base.BasePresenter;
import id.gagahib.cermati.data.payload.GitPayload;

public interface SearchUserPresenter extends BasePresenter {
    void doGetUsers(GitPayload payload);
}
