package id.gagahib.cermati.data.payload;

import com.google.gson.annotations.SerializedName;

public class GitPayload {

    @SerializedName("q")
    private String q;

    @SerializedName("page")
    private Integer page;

    public void setQ(String q) {
        this.q = q;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getQ() {
        return q;
    }

    public Integer getPage() {
        return page;
    }
}
