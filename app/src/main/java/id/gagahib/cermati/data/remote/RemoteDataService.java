
package id.gagahib.cermati.data.remote;

import id.gagahib.cermati.data.beanmodel.GitBean;
import id.gagahib.cermati.data.payload.GitPayload;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RemoteDataService {

    @GET("search/users")
    Call<GitBean> doGetUsers(@Query("q") String q,
                             @Query("page") Integer page);

}


